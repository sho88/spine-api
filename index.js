const process = require('process');
const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const service = require('feathers-knex');
const app = express(feathers());
const PORT = 8080;
const environment = process.env.NODE_ENV || 'development';
const { before } = require('./services');

// initialize the dotenv variables
require('dotenv').config();

const Model = require("knex")({
  client: process.env.DB_CLIENT,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
  }
});

app.use(express.json());
app.configure(express.rest());
app.use(express.errorHandler());

// apply middleware to the '/api' url
app.use("/api", (req, res, next) => {
  console.log(`Currently on the ${environment} environment.`);
  if (environment === 'local' || environment === 'development'){
    // also, along with that check, we can see if the user is authenticated or not - some PassportJS / JWT would need to be implemented here.
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Auth-Dev, CSRF-Token");
  }
  next();
});

app.use('/api/answers', service({ Model, name: 'answer' }));

app.use('/api/courses', service({ Model, name: 'course' }));
app.service('/api/courses').hooks({ before });

app.use('/api/enrol', service({ Model, name: 'courseenrol' }));
app.service('/api/enrol').hooks({ before });

app.use('/api/groups', service({ Model, name: 'groups' }));
app.use('/api/invitations', service({ Model, name: 'invitation' }));
app.use('/api/questions', service({ Model, name: 'question' }));
app.use('/api/tests', service({ Model, name: 'test' }));
app.use('/api/topics', service({ Model, name: 'topic' }));
app.use('/api/users', service({ Model, name: 'users' }));
app.use('/api/usertypes', service({ Model, name: 'usertype' }));
app.use('/api/usergroups', service({ Model, name: 'usergroup' }));
app.use('/api/history', service({ Model, name: 'watchhistory' }));

app.listen(PORT, () => console.log(`Listening on port: ${PORT}`));
