module.exports = {
  before: {
    create (ctx) {
      ctx.data = { ...ctx.data, created_at: Date.now() };
      return ctx;
    },
    update (ctx) {
      ctx.data = { ...ctx.data, updated_at: Date.now() };
      return ctx;
    }
  }
}